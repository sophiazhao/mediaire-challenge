# Color Tile Game

## How to run the program

To play the game, following generate, solve and play instructions.

### Generate new game

Using the `generate` command to generate a new game board

Arguments:

Name | Description
--- | ---
`--size` | the number of columns and rows of the board you want to generate.
`--colors` | how many colors on the board you want to play with.

``` 
python main.py generate --size=5 --colors=3 > board.txt
```

### Solve the game

Using the `solve` command to solve a game board

Arguments:

Name | Description
--- | ---
`--board` | path to a generated game board you want to solve.
`--colors` | how many colors are on the board.

```
python main.py solve --board=board.txt --colors=3 > moves.txt 
```

The game board and moves files are space separated text files.

### Play the game

Using the `play` command to play a game with a set of moves.

Arguments:

Name | Description
--- | ---
`--board` | path to a generated game board you want to solve.
`--moves` | path to a set of moves you want to play.

``` 
python main.py play --board=board.txt --moves=moves.txt
```

## Testing

Unit tests can be run using `pytest`.

```
pytest
```
