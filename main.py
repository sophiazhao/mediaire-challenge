import random
import argparse
import copy


def print_board(board):
    for row in board:
        for color in row:
            print(color, end=' ')
        print()


def load_board(board_path):
    board = []
    board_file = open(board_path, "r")
    for line in board_file:
        board.append([int(x.strip()) for x in line.strip().split(' ')])
    return board


def flood_fill(board, row, col, initial_color, replacement_color):
    node = board[row][col]
    if node == replacement_color:
        return
    elif node is not initial_color:
        return
    else:
        board[row][col] = replacement_color
    if row + 1 < len(board):
        flood_fill(board, row + 1, col, initial_color, replacement_color)
    if row - 1 >= 0:
        flood_fill(board, row - 1, col, initial_color, replacement_color)
    if col - 1 >= 0:
        flood_fill(board, row, col - 1, initial_color, replacement_color)
    if col +1 < len(board[row]):
        flood_fill(board, row, col + 1, initial_color, replacement_color)


def region_size(board, visited, row, col, color, size):
    visited[row][col] = 1
    if board[row][col] != color:
        return size

    if row + 1 < len(board) and visited[row + 1][col] != 1:
        size = region_size(board, visited, row + 1, col, color, size)
    if row - 1 >= 0 and visited[row - 1][col] != 1:
        size = region_size(board, visited, row - 1, col, color, size)
    if col - 1 >= 0 and visited[row][col - 1] != 1:
        size = region_size(board, visited, row, col - 1, color, size)
    if col +1 < len(board[row]) and visited[row][col + 1] != 1:
        size = region_size(board, visited, row, col + 1, color, size)

    return size + 1


def generate(args):
    board = [[random.randint(0, args.colors - 1) for i in range(args.size)] for j in range(args.size)]
    print_board(board)


def solve(args):
    board = load_board(args.board)

    while True:
        results = {}

        for color in range(args.colors):
            possible_board = copy.deepcopy(board)
            flood_fill(possible_board, 0, 0, board[0][0], color)
            visited = [[0 for i in range(len(possible_board))] for j in range(len(possible_board[0]))]
            solved = region_size(possible_board, visited, 0, 0, color, 0)
            if solved not in results:
                results[solved] = color

        best_result = sorted(results.keys(), reverse=True)[0]
        flood_fill(board, 0, 0, board[0][0], results[best_result])
        print(results[best_result], end=' ')
        if best_result == len(board) * len(board[0]):
            break
    print()


def play(args):
    moves_file = open(args.moves, "r")
    moves = [int(x.strip()) for x in moves_file.read().strip().split(' ')]

    board = load_board(args.board)

    print("Initial board")
    print_board(board)
    print()

    for i in range(len(moves)):
        color = moves[i]
        flood_fill(board, 0, 0, board[0][0], color)

        visited = [[0 for i in range(len(board))] for j in range(len(board[0]))]
        solved = region_size(board, visited, 0, 0, color, 0)

        print("Move:", i, "color:", color, 'solved:', solved)
        print_board(board)
        print()

        if solved == len(board) * len(board[0]):
            break


def main():
    parser = argparse.ArgumentParser(description='Color tile game')
    subparsers = parser.add_subparsers(required=True, dest='command')

    gen_command = subparsers.add_parser('generate')
    gen_command.add_argument("--size", type=int, required=True, help="The size of the game grip")
    gen_command.add_argument("--colors", type=int, required=True, help="How many color you want to play")

    solve_command = subparsers.add_parser('solve')
    solve_command.add_argument("--board", type=str, required=True, help="The game board to solve")
    solve_command.add_argument("--colors", type=int, required=True, help="How many color you want to play")

    play_command = subparsers.add_parser('play')
    play_command.add_argument("--board", type=str, required=True, help="The game board to play")
    play_command.add_argument("--moves", type=str, required=True, help="The moves you want to play")

    args = parser.parse_args()
    if args.command == 'generate':
        generate(args)
    elif args.command == 'solve':
        solve(args)
    elif args.command == 'play':
        play(args)


if __name__ == '__main__':
    main()
