from tempfile import *
import main


def test_region_size():
    board = [[0, 0, 3], [0, 1, 0], [1, 0, 1]]
    visited = [[0 for i in range(len(board))] for j in range(len(board[0]))]
    region_size = main.region_size(board, visited, 0, 0, 0, 0)
    assert region_size == 3


def test_flood_fill():
    board = [[0, 0, 3], [0, 1, 0], [1, 0, 1]]
    main.flood_fill(board, 0, 0, 0, 1)

    expected = [[1, 1, 3], [1, 1, 0], [1, 0, 1]]
    assert board == expected


def test_load_board():
    f = NamedTemporaryFile(delete=False, mode='w')
    f.write('0 0 3\n0 1 0\n 1 0 1\n')
    f.close()

    board = main.load_board(f.name)

    expected = [[0, 0, 3], [0, 1, 0], [1, 0, 1]]
    assert board == expected
